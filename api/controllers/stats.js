/**
 * Device controller
 */

'use strict';

var es = require('elasticsearch')
    , CONFIG = require('config')
    , distanceDetect = 50;

var findRecentRecordQuery = {
    index: CONFIG.ELASTIC.INDEX,
    type: CONFIG.ELASTIC.TYPE,
    body: {
        "query": {
            "filtered": {
                "query": {
                    "query_string": {
                        "query": "*",
                        "analyze_wildcard": true
                    }
                }
            }
        },
        "size": 1,
        "sort": [
            {
                "date": {
                    "order": "desc"
                }
            }
        ]
    }
};

var client = es.Client({
    host: CONFIG.ELASTIC.HOST.URL
});

module.exports = {
    getAvgHumidity: getAvgHumidity,
    getAvgLight: getAvgLight,
    getAvgTemperature: getAvgTemperature,
    getCurrentData: getCurrentData,
    getCurrentValue: getCurrentValue
}

/**
 * Returns average humidity
 *
 * @param req
 * @param res
 */
function getAvgHumidity(req, res) {

    console.log(CONFIG);

    return client.search({
        index: CONFIG.ELASTIC.INDEX,
        type: CONFIG.ELASTIC.TYPE,
        body: {
            "query": {
                "filtered": {
                    "query": {
                        "query_string": {
                            "query": "*",
                            "analyze_wildcard": true
                        }
                    }
                }
            },
            "size": 0,
            "aggs": {
                "1": {
                    "avg": {
                        "field": "humidity"
                    }
                }
            }
        }
    }).then(function (resp) {
        if ('value' in resp.aggregations["1"]) {
            res.json({value: resp.aggregations["1"].value});
        } else {
            res.json({message: 'An error has occured'});
        }
    });
}

/**
 * Returns average light
 *
 * @param req
 * @param res
 */
function getAvgLight(req, res) {

    return client.search({
        index: CONFIG.ELASTIC.INDEX,
        type: CONFIG.ELASTIC.TYPE,
        body: {
            "query": {
                "filtered": {
                    "query": {
                        "query_string": {
                            "query": "*",
                            "analyze_wildcard": true
                        }
                    }
                }
            },
            "size": 0,
            "aggs": {
                "1": {
                    "avg": {
                        "field": "light"
                    }
                }
            }
        }
    }).then(function (resp) {
        if ('value' in resp.aggregations["1"]) {
            res.json({value: resp.aggregations["1"].value});
        } else {
            res.json({message: 'An error has occured'});
        }
    });
}

/**
 * Returns average temperature
 *
 * @param req
 * @param res
 */
function getAvgTemperature(req, res) {

    return client.search({
        index: CONFIG.ELASTIC.INDEX,
        type: CONFIG.ELASTIC.TYPE,
        body: {
            "query": {
                "filtered": {
                    "query": {
                        "query_string": {
                            "query": "*",
                            "analyze_wildcard": true
                        }
                    }
                }
            },
            "size": 0,
            "aggs": {
                "1": {
                    "avg": {
                        "field": "temperature"
                    }
                }
            }
        }
    }).then(function (resp) {
        if ('value' in resp.aggregations["1"]) {
            res.json({value: resp.aggregations["1"].value});
        } else {
            res.json({message: 'An error has occured'});
        }
    });
}

/**
 * Returns current data
 *
 * @param req
 * @param res
 */
function getCurrentData(req, res) {

    return client.search(findRecentRecordQuery).then(function (resp) {
        var hits = resp.hits.hits;
        if (hits.length > 0) {
            var distance = parseFloat(hits[0]._source.distance);
            var data = {
                humidity: parseFloat(hits[0]._source.humidity),
                light: parseFloat(hits[0]._source.light),
                temperature: parseFloat(hits[0]._source.temperature),
                distance: distance,
                isToiletFree: (!(distance <= distanceDetect))
            };
            console.log(data);
            return res.json(data);
        } else {
            res.json({code: 404, message: "No results"});
        }
    }, function (err) {
        console.trace(err.message);
    });
}

function getCurrentValue(req, res) {
    var query = findRecentRecordQuery;
    var field = req.swagger.params.field.value;
    //query.terms = {"_field_names": [field]};
    if (!(field === "temperature" || field === "humidity" || field === "light" || field === "distance" || field === "isToiletFree"))  { // so lame ;p
        res.json({value: 0});
        return;
    }

    return client.search(query).then(function (resp) {
        var hits = resp.hits.hits;
        if (hits.length > 0) {
            var data = {"value": ""};
            if (field == "isToiletFree") {
                data.value = (!(parseFloat(hits[0]._source.distance) <= distanceDetect))
            } else {
                data.value = parseFloat(hits[0]._source[field]);
            }
            console.log(data);
            return res.json(data);
        } else {
            res.json({code: 404, message: "No results"});
        }
    }, function (err) {
        console.trace(err.message);
    });
}
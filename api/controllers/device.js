/**
 * Device controller
 */

'use strict';

var util = require('util')
    , es = require('elasticsearch')
    , moment = require('moment')
    , CONFIG = require('config');

var client = es.Client({
    host: CONFIG.ELASTIC.HOST.URL
    //sniffOnStart: true,
    //sniffInterval: 300000
});

module.exports = {
    index: index,
    sendData: sendData,
    create: create,
    read: read,
    update: update,
    remove: remove
}

/**
 * Returns index of devices about device
 * @param req
 * @param res
 */
function index(req, res) {
    res.json(output);
}

/**
 * Send data to platform
 *
 * @param req
 * @param res
 */
function sendData(req, res) {

    var humidity = parseFloat(req.swagger.params.device.value.humidity) || 0.0;
    var temperature = parseFloat(req.swagger.params.device.value.temperature) || 0.0;
    var light = parseInt(req.swagger.params.device.value.light) || 0;
    var duration = parseInt(req.swagger.params.device.value.duration) || 0;
    var distance = parseInt(req.swagger.params.device.value.distance) || 0;

    var output = {message: util.format('Humidity: %s, Temperature: %s, Light: %s', humidity, temperature, light)};

    // index a document
    client.index({
        index: CONFIG.ELASTIC.INDEX,
        type: CONFIG.ELASTIC.TYPE,
        //id: 1,
        body: {
            title: 'dev1',
            humidity: humidity,
            temperature: temperature,
            light: light / 10,
            duration: duration,
            distance: distance,
            date: moment().format()
        }
    }, function (err, resp) {
        if (err !== 'undefined') {
            //res.json(err);
        }
    });

    res.json(output);
}

/**
 * Returns the data for specific Device with the id of {id}
 *
 * @param req
 * @param res
 */
function read(req, res) {
    var id = req.swagger.params.id.value || undefined;

    return client.search({
        index: CONFIG.ELASTIC.INDEX,
        type: CONFIG.ELASTIC.TYPE,
        body: {
            query: {
                term: {
                    _id: id
                }
            }
        }
    }).then(function (resp) {
        var hits = resp.hits.hits;
        if (hits.length > 0) {
            return res.json(hits[0]._source);
        } else {
            res.json({code: 404, message: "No results"});
        }
    }, function (err) {
        console.trace(err.message);
    });
}

/**
 * Updates the infromation for specific Device
 *
 * @param req
 * @param res
 */
function update(req, res) {
    var id = req.swagger.params.id.value || undefined;

    return client.search({
        index: CONFIG.ELASTIC.INDEX,
        type: CONFIG.ELASTIC.TYPE,
        body: {
            query: {
                term: {
                    _id: id
                }
            }
        }
    }).then(function (resp) {
        var hits = resp.hits.hits;
        if (hits.length > 0) {
            return res.json(hits[0]._source);
        } else {
            res.json({code: 404, message: "No results"});
        }
    }, function (err) {
        console.trace(err.message);
    });
}

/**
 * Deletes the specific Device
 *
 * @param req
 * @param res
 */
function remove(req, res) {
    var id = req.swagger.params.id.value || undefined;

    return client.delete({
        index: CONFIG.ELASTIC.INDEX,
        type: CONFIG.ELASTIC.TYPE,
        id: id
    }, function (err, response) {
        if (err) {
            res.json({code: 500, message: 'An error has occured'});
        } else {
            res.json({code: 200});
        }
    });
}

/**
 * Creates a new Device
 *
 * @param req
 * @param res
 */
function create(req, res) {

}